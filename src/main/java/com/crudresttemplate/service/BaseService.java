package com.crudresttemplate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crudresttemplate.dao.impl.BaseDaoImpl;

@Service("baseService")
public class BaseService<T> {
	
	@Autowired
	BaseDaoImpl<T> baseDao;
	
	public void createEntity(T entity) {
		baseDao.create(entity);
	}
	
}
