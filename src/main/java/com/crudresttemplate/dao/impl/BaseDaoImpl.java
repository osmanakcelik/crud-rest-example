package com.crudresttemplate.dao.impl;

import java.lang.reflect.ParameterizedType;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public abstract class BaseDaoImpl<T> {
	
	@Autowired
	SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public SessionFactory getSessionFactory(){
		return this.sessionFactory;
	}
	
	@Transactional
	public void create(T entity) {
		Session session = sessionFactory.getCurrentSession();
		session.save(entity);
	}
	
	@Transactional
	public void update(T entity) {
        Session session = sessionFactory.getCurrentSession();
        session.update(entity);
        
    }
}
