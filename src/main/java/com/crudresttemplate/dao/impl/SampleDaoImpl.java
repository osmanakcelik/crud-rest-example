package com.crudresttemplate.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.crudresttemplate.entity.SampleEntity;

@Repository
public class SampleDaoImpl extends BaseDaoImpl<SampleEntity> {
	
	@Transactional
	public SampleEntity read(int id) {
		Session session = sessionFactory.getCurrentSession();
		return (SampleEntity) session.get(SampleEntity.class, id);
	}
	
	@Transactional
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		SampleEntity entity = (SampleEntity) session.load(SampleEntity.class, new Integer(id));
        session.delete(entity);
	}
	
	public List<SampleEntity> getAllUsers() {
        List<SampleEntity> users = new ArrayList<SampleEntity>();
        Session session = sessionFactory.getCurrentSession();
        users = session.createQuery("from SampleEntity").list();
        return users;
    }

	
	
}
