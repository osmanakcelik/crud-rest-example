package com.crudresttemplate.controller;

import java.util.List;

public interface BaseCRUDController<T> {
	
	void create(T model);
	
	T read(T model);
	
	T update(T model);
	
	void delete(T model);
	
	List<T> getlist(T model);
	
}
