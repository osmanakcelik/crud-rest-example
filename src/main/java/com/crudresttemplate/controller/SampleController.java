package com.crudresttemplate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.crudresttemplate.entity.SampleEntity;
import com.crudresttemplate.service.BaseService;

@Controller
@RequestMapping("/sample")
public class SampleController {
	
	@Autowired
	BaseService<SampleEntity> service;
	
	@RequestMapping(value="add/{text}", method = RequestMethod.GET)
	//@ResponseStatus(value = HttpStatus.OK)
	public void createSample(@PathVariable String text) {
		SampleEntity entity = new SampleEntity();
		entity.setText(text);
		service.createEntity(entity);
	}

}
